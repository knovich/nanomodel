program main

use TMM

implicit none


!Aux
integer i, j, jj, k
complex*32 tmp_m(2,2), aa, bb
real*16 errabs
!For guesses search
integer st_min_search, nofroots, min_count
real*16 step_re, step_im, min_u, max_u, mean_u
complex*32 prev_val, cur_val, cur_X, prev_X
!For initial guesses and accurate roots
complex*32, allocatable :: init_guesses(:), roots(:), new_roots(:)

logical is_ok


call read_config()
call read_data()
write(*,*)'Name of roots file (assuming roots.dta)'
read(*,'(A)')fn_roots
if(fn_roots .EQ. '')then
    fn_roots='roots.dta'
end if
open(8,FILE=fn_roots,FORM='UNFORMATTED')
write(8)e_num

do k=1,e_num
    !
    !Start with making plain arrays (U, D, mass) out of complex structure from data file
    !
    call make_plain_arrays(k)
    write(8)e_field
    write(8)N
    write(8)mass,U,D
    !--temporary HACK
    !U(N-1:N)=U(0)

    !
    !Determine constrains of eigenenergies search
    !
    max_u=maxval(U(0:N))
    min_u=minval(U(0:N))
    mean_u=(min_u+max_u)/2
    min_u=max(mean_u-start_e,min_u)
    max_u=min(mean_u+finish_e,max_u)
    step_re=(max_u-min_u)/qext(re_steps)
    step_im=Eim_max/qext(im_steps)

    !
    !Searching roots approximations
    !
    nofroots=0
    cur_x=qcmplx(min_u,0q0)
    cur_val=eqn(cur_X)
    st_min_search=0
    do i=1,re_steps
        prev_val=cur_val
        prev_X=cur_X
        cur_X=qcmplx(min_u+qext(i)*step_re,0q0)
        cur_val=eqn(cur_x)
        if(st_min_search.EQ.1)then
            if(abs(cur_val).GT.abs(prev_val))then
                !found local minimum finally
                st_min_search=0
                !estimate imaginary part
                write(*,*)'Starting im part estimation'
                if(qimag(cur_val).NE.0q0 .AND. qimag(prev_val).NE.0q0)then
                    !The result of imaginary part estimation is in prev_x!!!!
                    min_count=-1
                    call estimate_im(prev_x, min_count, Eim_max)
                    !prev_X=qcmplx(qreal(prev_X),-Eim_max)
                end if
                nofroots=nofroots+1
                call add_guess(nofroots, init_guesses, prev_X)
            end if
        elseif (abs(cur_val).LT.abs(prev_val))then
            !approaching local minimum, time to start searching
            st_min_search=1
        end if
        if(nofroots.EQ.last_root)then
            exit
        end if
    end do
    
    write(*,*)nofroots
    if(last_root.GT.nofroots)then
        last_root=nofroots
    end if
    !fake num of roots
    first_root=1
    i=last_root-first_root+1
    !write(8)i
    write(*,*)init_guesses
    write(*,*)k
    
    allocate(roots(1:nofroots),new_roots(1:nofroots))
    j=1
    do i=first_root,last_root
        write(*,*)i
        !if(qimag(init_guesses(i)).EQ.0q0)then
        if(2==1)then
            aa=init_guesses(i)-step_re
            bb=init_guesses(i)+step_re
            call allocate_k_vec(init_guesses(i))
            tmp_m=T_prod(N-1)
            errabs=1d-3*abs(tmp_m(1,2))
            call find_root(eqna, aa, bb, errabs)
            roots(i)=bb
            write(8)roots(i)
        else
            call allocate_k_vec(init_guesses(i))
            tmp_m=T_prod(get_constraint_layer(init_guesses(i))-1)
            errabs=1d-10*abs(tmp_m(1,2))
            is_ok=.true.
            call newton(init_guesses(i), errabs, roots(i), is_ok)
            if(is_ok == .false.)then
                nofroots=nofroots-1
            else
                new_roots(j)=roots(i)
                j=j+1
            end if
            !roots(i)=init_guesses(i)
            !call find_abs_min(eqnc, roots(i), errabs, aa)
        end if
    end do
    write(8)nofroots
    do i=1,nofroots
        write(8)new_roots(i)
    end do
    deallocate(roots,new_roots,mass,D,U,init_guesses)

end do


write(*,*)'finale'
close(8)
read(*,*)


contains

!subroutine make_net(left)
!    complex*32 left, t
!    integer i,j
!    
!    open(9,name='net.txt')
!    do i=0,20
!    do j=0,10
!        t=qcmplx(qreal(left)+step_re*qext(i-10)/20.,qimag(left)+qimag(left)/100.*qext(j-5)/10.)
!        write(9,"(3E18.7E3)")qreal(t),qimag(t),eqnc(t)
!    end do
!    end do
!    write(*,*)'HELL YEAH!'
!    close(9)
!end subroutine make_net

real*16 function eqna(e)
    complex*32 e
    complex*32 tmp,tmp2
    
    tmp2=eqn(e)
    eqna=qreal(tmp2)
    
end function eqna


complex*16 function eqnb(e)
    complex*16 e
    complex*32 tmp,tmp2
    
    tmp=qcmplx(e)
    tmp2=eqn(tmp)
    eqnb=cmplx(tmp2)
    
end function eqnb


real*16 function eqnc(e)
    complex*32 e
    complex*32 tmp,tmp2
    
    tmp2=eqn(e)
    eqnc=abs(tmp2)
    
end function eqnc


end program main