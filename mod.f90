module TMM

implicit none



type layer_type
    integer size, num
    character*20 name
    real*16,allocatable::mass(:)
    real*16,allocatable::D(:)
    real*16,allocatable::U(:)
end type layer_type

type (layer_type),allocatable::layers(:)

integer layer_kinds
character*20 layer

type f_grad
    complex*32 f,grad
    real*16 err
end type f_grad

type constraint
    real*16 val
    integer layer
end type constraint

type(constraint),allocatable::constr_array(:)

complex*32, allocatable :: k_vec(:)
real*16, allocatable :: mass(:), D(:), U(:)
real*16 e_field, hbar, Eim_max, mean_E
real*16 start_e, finish_e, scale_coeff, chop_coeff, e_casc_num
real*16 str_length
integer N, e_steps, max_newton_iter, discr, im_steps, re_steps, first_root, last_root, e_num, need_mean_e, chop_wave, localize_states
real*16, allocatable :: e_field_arr(:)
character*80 fn_roots

contains

!!!!!!!!!
!!!!!!!!!
!Eigen funcs
!
!


subroutine chop_out(arr_out, nofr)
    real*16,allocatable::arr_out(:,:)
    integer i, j, nofr
    real*16 thr
    
    do i=1,nofr
        thr=maxval(arr_out(i,:))*chop_coeff
        j=1
        do while (arr_out(i,j) .LT. thr)
            arr_out(i,j)=-1.0/0.0
            j=j+1
        end do
        j=discr
        do while (arr_out(i,j) .LT. thr)
            arr_out(i,j)=-1.0/0.0
            j=j-1
        end do
    end do
end subroutine chop_out


real*16 function peak_x(fun,f_num)
    complex*32, allocatable::fun(:,:)
    complex*32 max
    integer f_num
    integer i
    max=(0q0,0q0)
    
    do i=1,discr
        if(abs(fun(f_num,i)).GE.abs(max))then
            peak_x=str_length*qext(i)/qext(discr)
            max=fun(f_num,i)
        end if
    end do
end function peak_x


real*16 function peak_val(fun,f_num)
    real*16, allocatable::fun(:,:)
    real*16 max
    integer f_num
    integer i
    
    max=0q0
    do i=1,discr
        if(abs(fun(f_num,i)).GE.abs(max))then
            peak_val=fun(f_num,i)
            max=fun(f_num,i)
        end if
    end do
end function peak_val


subroutine add_eifu(nofr, fu, val)
    integer nofr
    complex*32 val
    complex*32, allocatable :: fu(:,:), futmp(:,:), func(:)

    if(nofr .GT. 1)then
        allocate(futmp(nofr-1,discr))
        futmp(:,:)=0q0
        if(allocated(fu))then
            futmp=fu
            deallocate(fu)
        end if
        allocate(fu(nofr,discr))
        fu(:,:)=0q0
        fu(1:nofr-1,1:discr)=futmp
        allocate(func(discr))
        call eigenfunc(val, discr, func)
        fu(nofr,1:discr)=func
        deallocate(futmp)
        deallocate(func)
     else
        allocate(fu(1,discr))
        allocate(func(discr))
        call eigenfunc(val, discr, func)
        fu(1,1:discr)=func
        deallocate(func)
     end if
end subroutine add_eifu


complex*32 function mat_el(ef,fu1,fu2,sq)
    complex*32, allocatable :: ef(:,:)
    real*16 dx,x
    integer i, fu1, fu2, sq 
    
    dx=sum(D(0:N))/size(ef, 2)
    mat_el=(0q0,0q0)
    x=dx
    do i=1,size(ef, 2)
        mat_el=mat_el+conjg(ef(fu1,i))*ef(fu2,i)*qcmplx(x**sq*dx)
        x=x+dx
    end do
end function mat_el


real*16 function mean_x(ef,fu1,sq)
    complex*32, allocatable :: ef(:,:)
    real*16 dx,x
    integer i, fu1,sq
    
    dx=sum(D(0:N))/size(ef, 2)
    mean_x=0q0
    x=dx
    do i=1,size(ef, 2)
        mean_x=mean_x+abs(ef(fu1,i))**2*x**sq*dx
        x=x+dx
    end do
end function mean_x


subroutine eigenfunc(e, dd, f)
    real*16 x, sx, dx, cl, norm, length, stop_x, Dsum
    complex*32 e
    complex*32, allocatable :: f(:),a(:),b(:)
    complex*32 tuple(2), ctmp1,ctmp2, t_m(2,2)
    integer dd, i, l, stop_l
    
    length=sum(D(0:N))
    allocate(a(0:N),b(0:N))
    dx=length/qext(dd)
    a(0)=(0q0,0q0)
    b(0)=(1q0,0q0)
    call allocate_k_vec(e)
    do i=1,N
        tuple=(/a(i-1),b(i-1)/)
        tuple=matmul( Transfer_Matrix(i-1),tuple)!reshape((/a(i-1),b(i-1)/),(/2,1/)) )
        !t_m=T_prod(i-1)
        a(i)=tuple(1)
        b(i)=tuple(2)
    end do
    stop_x=length
    i=N
    do while (i .GT. 0)
        if(U(i).GT.qreal(e))then
            exit
        else
            stop_x=stop_x-D(i)
            i=i-1
        end if
    end do
!    stop_x=stop_x+100
    !a(N)=qcmplx(0q0,0q0)


    !this may cause a leap at the last border if accuracy is poor
    !b(N)=qcmplx(0q0,0q0)
    x=0q0
    sx=0q0
    l=0
    i=1
    norm=0q0
    Dsum=D(0)
    if(localize_states == 1)then
        stop_l=get_constraint_layer(e)
    else
        stop_l=N
    end if
    do while (x .LE. length)
        do while (x .GT. Dsum)
            l=l+1
            Dsum=Dsum+D(l)
        end do
        sx=x-Dsum+D(l)
        if(((x.GE.stop_x) .AND. (chop_wave .EQ.1)) .or. (l >= stop_l))then
            f(i)=(0q0,0q0)
        else
            if(k_vec(l).NE.(0q0,0q0))then
                !f(i)=a(l)*exp(k_vec(l)*qcmplx(sx,0q0))+b(l)*exp(-k_vec(l)*qcmplx(sx,0q0))
                f(i)=a(l)*exp(k_vec(l)*qcmplx(0q0,sx))+b(l)*exp(-k_vec(l)*qcmplx(0q0,sx))
            else
                f(i)=a(l)*qcmplx(sx,0q0)+b(l)
            end if
        end if
        norm=norm+(cqabs(f(i))**2)*dx
        x=x+dx
        if(i.LT.dd)then
            i=i+1
        else
            exit
        end if
    end do
    do i=1,dd
        f(i)=f(i)/qcmplx(sqrt(norm),0q0)
    end do
    norm=0q0
    do i=1,dd
        norm=norm+(cqabs(f(i))**2)*dx
    end do    
end subroutine eigenfunc



!!!!!!!!!!!
!!!!!!!!!!!
!Reading data
!
!


subroutine read_data()
    integer i,j
    character*80 fn
    real*16 e_len
    
    write(*,*)'Name of data file (assuming data.txt)'
    read(*,'(A)')fn
    if(fn .EQ. '')then
        fn='data.txt'
    end if
    open(1,FILE=fn)
    read(1,*) layer_kinds, e_num
    allocate(layers(1:layer_kinds))
    allocate(e_field_arr(1:e_num))
    read(1,*)e_field_arr
    read(1,'(A)')layer
    i=1
    e_len=0q0
    mean_E=0q0
    do while (layer .NE. 'end')
        layers(i)%name=layer
        read(1,*)layers(i)%size,layers(i)%num
        allocate(layers(i)%mass(1:layers(i)%size))
        allocate(layers(i)%U(1:layers(i)%size))
        allocate(layers(i)%D(1:layers(i)%size))
        read(1,*)layers(i)%mass
        read(1,*)layers(i)%U
        read(1,*)layers(i)%D
        if(i .LE. e_casc_num)then
            e_len=e_len+sum(layers(i)%D)
        end if
        if(layer .EQ. 'SL')then
            mean_E=sum((/(layers(i)%U(j)*layers(i)%D(j),j=1,layers(i)%size)/))/sum(layers(i)%D)
        end if
        read(1,'(A)')layer
        i=i+1
    end do
    if(e_casc_num .NE. 0)then
        e_field_arr=e_field_arr/e_len
    end if
    close(1)    
end subroutine read_data


subroutine read_config()
    character*80 fn
    
	write(*,*)'Name of configuration file (assuming config.txt)'
    read(*,'(A)')fn
    if(fn .EQ. '')then
        fn='config.txt'
    end if
    open(1,FILE=fn)
    read(1,*) hbar, re_steps, im_steps, e_steps, max_newton_iter, Eim_max, discr, start_e, finish_e,&
		first_root, last_root, scale_coeff, chop_coeff, e_casc_num, need_mean_e, chop_wave, localize_states
	close(1)
end subroutine read_config


subroutine make_plain_arrays(k)
    integer k,jj,i,m
    
    N=sum((/(layers(i)%size * layers(i)%num,i=1,layer_kinds)/))-1
    allocate(mass(0:N), U(0:N), D(0:N))
    if(need_mean_e .EQ. 1)then
        U(0)=mean_E
        U(N)=mean_E
    end if
    jj=-1
    do i=1,layer_kinds
    do m=1,layers(i)%num
        mass((jj+1):(jj+layers(i)%size))=layers(i)%mass
        U((jj+1):(jj+layers(i)%size))=layers(i)%U
        D((jj+1):(jj+layers(i)%size))=layers(i)%D
        jj=jj+layers(i)%size
    end do
    end do

    e_field=e_field_arr(k)
    
    if(e_field .NE. 0q0)then
        call apply_field()
    end if
    
    str_length=sum(D(0:N))
end subroutine make_plain_arrays


subroutine apply_field()
    real*16, allocatable :: tmp_D(:), tmp_U(:), tmp_mass(:)
    real*16 x, sx
    integer i,j
    
    allocate(tmp_D(0:N),tmp_U(0:N),tmp_mass(0:N))
    tmp_D=D
    tmp_U=U
    tmp_mass=mass
    deallocate(D,U,mass)
    allocate(D(0:(e_steps*(N-1)+1)),U(0:(e_steps*(N-1)+1)),mass(0:(e_steps*(N-1)+1)))
    D(0)=tmp_D(0)
    D(e_steps*(N-1)+1)=tmp_D(N)
    U(0)=tmp_U(0)
    U(e_steps*(N-1)+1)=tmp_U(N)-e_field*sum(tmp_D(1:N-1))
    mass(0)=tmp_mass(0)
    mass(e_steps*(N-1)+1)=tmp_mass(N)
    do i=1,N-1
        x=sum(tmp_D(1:i-1))
        sx=tmp_D(i)/qext(e_steps)
        do j=1,e_steps
            x=x+sx
            D((i-1)*e_steps+j)=sx
            U((i-1)*e_steps+j)=tmp_U(i)-e_field*x
            mass((i-1)*e_steps+j)=tmp_mass(i)
        end do
    end do
    N=e_steps*(N-1)+1
    deallocate(tmp_D,tmp_U,tmp_mass)    
end subroutine apply_field



!!!!!!!!
!!!!!!!!
!Root finding
!
!


recursive subroutine estimate_im(prev, count, range)
    complex*32 prev, cur
    integer count, i, lmp
    real*16 range, step, m_abs, cv
    
    !prev=qcmplx(qreal(prev),-range)
    if(range .LT. 1q-50)then
        write(*,*)'Very small im part estimated'
        return
    end if
    m_abs=abs(eqn(prev))
    lmp=count
    count=0
    step=range/qext(im_steps)
    cur=prev+qcmplx(0q0,-step)
    cv=abs(eqn(cur))
    if ( cv .GT. m_abs ) then
        call estimate_im(prev, count, step)
        return
    else
        m_abs=cv
        do i=2,im_steps
            cur=cur+qcmplx(0q0,-step)
            cv=abs(eqn(cur))
            if( abs(cv) .LT. m_abs)then
                lmp=i
                m_abs=cv
            else
                prev=cur-qcmplx(0q0,-2*step)
                call estimate_im(prev, count, 2*step)
                return
            end if
        end do
    end if
    if(lmp.EQ.im_steps)then
        call estimate_im(prev, count, range)
    else
        write(*,*)'It shouldn''t be like this!'
    end if    
end subroutine estimate_im


subroutine add_guess(nofr, guesses, val)
    integer nofr
    complex*32 val
    complex*32, allocatable :: guesses(:), temp_g(:)

    if(nofr .GT. 1)then
        allocate(temp_g(nofr-1))
        temp_g=guesses(1:nofr-1)
        deallocate(guesses)
        allocate(guesses(nofr))
        guesses(1:nofr-1)=temp_g
        deallocate(temp_g)
        guesses(nofr)=val
     else
        allocate(guesses(1))
        guesses(1)=val
     end if
end subroutine add_guess


integer function r_sign(a,b)
    complex*32 a,b
    
    r_sign=0
    if(((qreal(a).LT.0q0)  .AND. (qreal(b).GT.0q0)).OR.((qreal(a).GT.0q0)  .AND. (qreal(b).LT.0q0)))then
    r_sign=1
    end if
end function r_sign


integer function c_sign(a,b)
    complex*32 a,b
    
    c_sign=0
    if(((qreal(a).LT.0q0)  .AND. (qreal(b).GT.0q0)).OR.((qreal(a).GT.0q0)  .AND. (qreal(b).LT.0q0)))then
    if(((qimag(a).LE.0q0)  .AND. (qimag(b).GE.0q0)).OR.((qimag(a).GE.0q0)  .AND. (qimag(b).LE.0q0)))then
    c_sign=1
    end if
    end if    
end function c_sign


subroutine find_root(f, a, b, err)
    real*16,external :: f
    real*16 err, fm, factor
    complex*32 middle, a, b, t1, t2
    
    t1=qcmplx(f(a),0q0)
    t2=qcmplx(f(b),0q0)
    if(r_sign(t1,t2).NE.1)then
        write(*,*)'No sign change around supposed root!'
    end if
    if(qimag(b).NE.0q0)then
        write(*,*)'Im part of supposed root is non-zero'
    end if
    if(f(a).LT.0q0)then
        factor=1q0
    else
        factor=-1q0
    end if
    middle=(a+b)/2q0
    fm=f(middle)
    do while (abs(fm).GT.err)
        if((fm*factor).GT.0q0)then
            b=middle
            middle=(a+middle)/2q0
        else
            a=middle
            middle=(middle+b)/2q0
        end if
        fm=f(middle)
        if((middle .EQ. a).OR.(middle .EQ. b))then
            print *, 'Reached the precision limit in find_root'
            exit
        end if
    end do
    b=middle    
end subroutine find_root



subroutine newton(gs, err, res, io)
    complex*32 gs
    real*16 err
    complex*32 res
    integer i,j
    logical io
    type(f_grad) tmp, prev

    res=gs
    i=0
    j=0
    tmp=f_with_grad(res)
    err=abs(tmp%err)
    do while (abs(tmp%f) > err)
        res=res-(tmp%f)/(tmp%grad)
        i=i+1
        prev=tmp
        tmp=f_with_grad(res)
        err=abs(tmp%err)
        if(i > max_newton_iter)then
            write(*,*)'Failed to converge:',abs(prev%f),' vs ',err
            exit
        end if
        if(abs(prev%f) < abs(tmp%f))then
            if(c_sign(tmp%f, prev%f).NE.1  .and.  c_sign(tmp%grad, prev%grad)==1)then
                write(*,*)"That's not a root"
                io=.false.
                exit
            end if
            j=j+1
            if(j > 10)then
                write(*,*)'Enough is enough!'
                exit
            end if
        end if
    end do
end subroutine newton



!!!!!!!!
!!!!!!!!
!Core functions
!
!


function Transfer_Matrix(k) result (tmijk) !i=1..2, j=1..2, k=1..N
    integer k
    complex*32 tmijk(2,2), kd, kd_res, ratio_pl, ratio_mn
    if(k_vec(k+1).EQ.(0q0,0q0))then
        write(*,*)'Iteration energy equal to one of U(i)'
        tmijk(1,1)=exp(k_vec(k)*qcmplx(D(k),0q0))*qcmplx(mass(k+1)/mass(k),0q0)*k_vec(k)
        tmijk(1,2)=-exp(-k_vec(k)*qcmplx(D(k),0q0))*qcmplx(mass(k+1)/mass(k),0q0)*k_vec(k)
        tmijk(2,1)=exp(k_vec(k)*qcmplx(D(k),0q0))
        tmijk(2,2)=exp(-k_vec(k)*qcmplx(D(k),0q0))
        return
    end if
    if(k_vec(k).EQ.(0q0,0q0))then
        write(*,*)'Iteration energy equal to one of U(i)'
        tmijk(1,1)=qcmplx(D(k)/2q0,0d0)+qcmplx(mass(k+1)/mass(k)/2q0,0q0)/k_vec(k+1)
        tmijk(1,2)=(0.5q0,0q0)
        tmijk(2,1)=qcmplx(D(k)/2q0,0d0)-qcmplx(mass(k+1)/mass(k)/2q0,0q0)/k_vec(k+1)
        tmijk(2,2)=(0.5q0,0q0)
        return
    end if
    !ATTE
    kd=exp(k_vec(k)*qcmplx(0q0,D(k)))
    kd_res=exp(k_vec(k)*qcmplx(0q0,-D(k)))
    ratio_pl=(0.5q0,0q0)+qcmplx(mass(k+1)/mass(k)/2q0,0q0)*k_vec(k)/k_vec(k+1)
    ratio_mn=(0.5q0,0q0)-qcmplx(mass(k+1)/mass(k)/2q0,0q0)*k_vec(k)/k_vec(k+1)
    tmijk(1,1)=kd*ratio_pl
    tmijk(1,2)=kd_res*ratio_mn
    tmijk(2,1)=kd*ratio_mn
    tmijk(2,2)=kd_res*ratio_pl
end function Transfer_Matrix


function TM_der(k) result (tmijk) !i=1..2, j=1..2, k=1..N
    integer k
    complex*32 tmijk(2,2), kd, kd_res, denom, a, b, c, dd

    kd=exp(k_vec(k)*qcmplx(0q0,D(k)))
    kd_res=exp(k_vec(k)*qcmplx(0q0,-D(k)))

    denom=hbar**2*mass(k)*k_vec(k)*k_vec(k+1)**3
    a=mass(k+1)**2*k_vec(k)**2
    b=mass(k)*mass(k+1)*k_vec(k+1)**2
    c=b*qcmplx(0q0,D(k))*k_vec(k)
    dd=qcmplx(0q0,D(k))*k_vec(k+1)**3*mass(k)*2

    tmijk(1,1)=kd/denom*(-a+b+c+dd)
    tmijk(1,2)=kd_res/denom*(a-b+c-dd)
    tmijk(2,1)=kd/denom*(a-b-c+dd)
    tmijk(2,2)=kd_res/denom*(-a+b-c-dd)
end function TM_der


function T_prod(k) result (t_p)
    complex*32 t_p(2,2), tmp(2,2), tm(2,2)
    integer k, i
    
    t_p=Transfer_Matrix(0)
    do i=1,k
        tm=Transfer_Matrix(i)
        t_p=matmul(tm,t_p)
    end do    
end function T_prod


function f_with_grad(e) result(res)
    type(f_grad) res
    integer i
    complex*32 e
    complex*32 prev_matr(2,2), prev_der(2,2), cur_matr(2,2), cur_der(2,2), tmp_matr(2,2), tmp_der(2,2)

    call allocate_k_vec(e)
    prev_matr=Transfer_Matrix(0)
    prev_der=TM_der(0)
    do i=1,N-1
        cur_matr=Transfer_Matrix(i)
        cur_der=TM_der(i)
        tmp_matr=matmul(cur_matr,prev_matr)
        tmp_der=matmul(cur_der,prev_matr)+matmul(cur_matr,prev_der)
        prev_matr=tmp_matr
        prev_der=tmp_der
    end do

    res%f=prev_matr(2,2)
    res%grad=prev_der(2,2)
    res%err=qreal(prev_matr(1,2)*exp((0q0,200q0)*k_vec(N)))
end function f_with_grad



!complex*32 function eqn(e) result(eqn_)
!    use LINCG_INT
!    complex*32 e
!    complex*32 t_p(2,2)
!    
!    call allocate_k_vec(e)
!    t_p=T_prod(N-1)
!    if(sum(t_p).EQ.sum(t_p))then
!        call LINCG(t_p, t_p)
!    end if
!    eqn_=t_p(2,2)
!
!end function eqn


complex*32 function eqn(e) result(eqn_)
    complex*32 e
    complex*32 t_p(2,2)
    integer constr_n
    
    call allocate_k_vec(e)
    if(localize_states == 1)then
        constr_n=get_constraint_layer(e)
    else
        constr_n=N
    end if
    t_p=T_prod(constr_n-1)
!    if(qreal(e).LT.U(N))then
!        eqn_=t_p(1,1)
!    else
!        eqn_=t_p(2,1)
!    end if
    !ATTE
    eqn_=t_p(2,2)
end function eqn


integer function get_constraint_layer(e) result(layer)
    complex*32 e
    integer i

    do i=N,1,-1
        if(U(i) > qreal(e))then
            layer=i
            exit
        end if
    end do
end function


subroutine allocate_k_vec(e)
    complex*32 e, tmp
    integer i
    
    if(allocated(k_vec))then
        deallocate(k_vec)
    end if
    allocate(k_vec(0:N))
    do i=0,N
        tmp=qcmplx(2q0*mass(i),0q0)*(e-qcmplx(U(i),0q0))
        k_vec(i)=cqsqrt(tmp)/qcmplx(hbar,0q0)
    end do    
end subroutine allocate_k_vec


end module TMM