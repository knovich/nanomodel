program main

use TMM
implicit none
complex*32,allocatable::e_funcs(:,:), roots(:)
real*16,allocatable::coords(:),U_prof(:),e_funcs_out(:,:),max_v(:),places(:),widths(:)
complex*32 cur_x
integer nofroots,k,i,l,jj
character name_head*4, name_tail*(*), name*14, combname*14, cn_head*6,fmt_comb*120
parameter (name_tail='.txt')
character mename_head*5, mename_tail*(*), mename*14
parameter (mename_tail='.txt')
character out_name*8

real*16 x,sx,dx, lrt, Dsum


call read_config()
!call read_data()

write(*,*)'Name of roots file (assuming roots.dta)'
read(*,'(A)')fn_roots
if(fn_roots .EQ. '')then
    fn_roots='roots.dta'
end if
open(1,FILE=fn_roots,FORM='UNFORMATTED')
read(1)e_num
write(*,*)'Enter desired 4-char name prefix (assuming ''',fn_roots(1:4),''')'
read(*,'(A)')name_head
if(name_head .EQ. '')then
    name_head=fn_roots(1:4)
end if
write(*,*)'Enter desired 6-char name prefix (assuming ''co',fn_roots(1:4),''')'
read(*,'(A)')cn_head
if(cn_head .EQ. '')then
    cn_head(1:2)='co'
    cn_head(3:6)=fn_roots(1:4)
end if
write(*,*)'Enter desired 5-char matrix elements name prefix (assuming ''m',fn_roots(1:4),''')'
read(*,'(A)')mename_head
if(mename_head .EQ. '')then
    mename_head(1:1)='m'
    mename_head(2:5)=fn_roots(1:4)
end if
out_name(1:4)=fn_roots(1:4)
out_name(5:8)='.txt'
open(4,FILE=out_name)

!saving last_root var value for future
lrt=last_root

do k=1,e_num
    write(*,*)k
    read(1)e_field
    read(1)N
    allocate(mass(0:N), U(0:N), D(0:N))
    read(1)mass,U,D
    str_length=sum(D(0:N))
    read(1)nofroots

    if(nofroots.LT.last_root)then
        last_root=nofroots
    end if    
    allocate(roots(1:nofroots),places(1:nofroots),widths(1:nofroots))
    
    !
    !Energy profile for output
    !
    x=0q0
    sx=0q0
    l=0
    i=1
    dx=str_length/qext(discr)
    allocate(coords(1:discr),U_prof(1:discr))
    Dsum=D(0)
    do while (x .LE. str_length)
        do while (x .GT. Dsum)
            l=l+1
            Dsum=Dsum+D(l)
        end do
        sx=x-Dsum+D(l)
        coords(i)=x
        U_prof(i)=U(l)
        x=x+dx
        if(i.LT.discr)then
            i=i+1
        else
            exit
        end if
    end do

    write(combname,"(A6,I0.2,A4)")cn_head,k,name_tail
    open(6,FILE=combname)
    write(fmt_comb,*)'(2E14.5,',nofroots,'E16.5E3)'
    
    write(4,*)k
    do i=1,nofroots
        read(1)roots(i)
        write(*,*)i
        cur_x=roots(i)
        call add_eifu(i, e_funcs, cur_X)
        places(i)=mean_x(e_funcs,i,1)
        widths(i)=sqrt(mean_x(e_funcs,i,2)-places(i)**2)
        write(name,"(A4,I0.2,A1,I0.3,A4)")name_head,k,'_',i,name_tail
        !open(5,FILE=name)
        !write(5,"(2E16.5E3)")e_funcs(i,:)
        !close(5)
        write(4,"(I3,5E18.7E3)")i, cur_x, peak_x(e_funcs,i),places(i),widths(i)
    end do
    write(4,*)
    
    write(6,fmt_comb)1,e_field,(/(qreal(roots(jj)),jj=1,nofroots)/)
    allocate(e_funcs_out(1:nofroots,1:discr))
    allocate(max_v(1:nofroots))
    do i=1,discr
    do jj=1,nofroots
        e_funcs_out(jj,i)=abs(e_funcs(jj,i))**2
    end do
    end do
    max_v=(/(maxval(e_funcs_out(jj,:)),jj=1,nofroots)/)
    if(chop_coeff .GT. 0q0)then
        call chop_out(e_funcs_out,nofroots)
    end if
    do i=1,discr
        write(6,fmt_comb)coords(i),U_prof(i),(/(e_funcs_out(jj,i)*scale_coeff/max_v(jj)+qreal(roots(jj)),jj=1,nofroots)/)
    end do
    deallocate(max_v)

    do i=0,0
        write(mename,"(A5,I0.2,A1,I0.2,A4)")mename_head,k,'_',i,mename_tail
        open(14+i,FILE=mename)
    end do
    do jj=1,20
        do i=first_root,last_root-jj
            !write(14+jj,"(3E16.5E3,2I4,2E16.5E3)")qreal(roots(i+jj)-roots(i)), qreal(roots(i)), qreal(roots(i+jj)), i, i+jj, abs(mat_el(e_funcs,i,i+jj,1))**2, abs(mat_el(e_funcs,i,i+jj,0))**2
            write(14,"(3E16.5E3,2I4,2E16.5E3)")qreal(roots(i+jj)-roots(i)), qreal(roots(i)), qreal(roots(i+jj)), i, i+jj, abs(mat_el(e_funcs,i,i+jj,1))**2, abs(mat_el(e_funcs,i,i+jj,0))**2
        end do
    end do
    do i=0,00
        close(14+i)
    end do
    
    deallocate(roots,U,D,e_funcs,mass,coords,U_prof,e_funcs_out,places,widths)
    last_root=lrt
    close(6)

end do
close(1)
close(4)

end program main